
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <float.h>
#include <stdbool.h>

#include "ocl_utils.h"

#define VERSION "0.2"

#define ITERS 10

#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#define MAX(a,b) (((a) > (b)) ? (a) : (b))

struct cell_id
{
  unsigned int i, j;
};

struct plane
{
  unsigned int num_cells;
  struct cell_id * cells;
};

void init_planes(const unsigned int dimx, const unsigned int dimy, struct plane ** planes, unsigned int *num_planes);
void print_planes(struct plane * planes, unsigned int num_planes);
void copy_planes(cl_mem *d_planes, struct plane *planes, unsigned int num_planes, cl_context context);
double wtime(void);

int main(int argc, char **argv)
{

  printf("Data Order Benchmark\n");
  printf("Version %s\n", VERSION);
  printf("\n");

  Arguments args;
  args.device_index = 0;
  args.cell_work = 64;
  args.dimx = 512;
  args.dimy = 512;

  parse_arguments(argc, argv, &args);

  printf("Work per cell: %zu\n", args.cell_work);
  printf("Grid: %zu x %zu\n", args.dimx, args.dimy);
  printf("Memory footprint %.1lf MB\n",
    (2.0*sizeof(double)*args.cell_work*args.dimx*args.dimy
    + 2.0*sizeof(double)*args.cell_work*args.dimx*args.dimy*4.0)
    / 1.0E6
  );


  // Init OpenCL
  cl_int err;
  unsigned num_devices;
  cl_device_id devices[MAX_DEVICES];
  num_devices = get_device_list(devices);
  if (args.device_index >= num_devices)
  {
    fprintf(stderr, "Invalid device index\n");
    return EXIT_FAILURE;
  }

  cl_device_id device = devices[args.device_index];
  char name[MAX_INFO_STRING];
  clGetDeviceInfo(device, CL_DEVICE_NAME, MAX_INFO_STRING, name, NULL);
  printf("Using device: %s\n", name);

  cl_context context = clCreateContext(0, 1, &device, NULL, NULL, &err);
  check_error(err, "Creating context");

  cl_command_queue queue = clCreateCommandQueue(context, device, 0, &err);
  check_error(err, "Creating queue");

  const char *kernel_string = get_kernel_string("kernels.cl");
  cl_program program = clCreateProgramWithSource(context, 1, &kernel_string, NULL, &err);
  check_error(err, "Creating program");

  char options[2048];
  sprintf(options, "-DCELL_WORK=%zu -DDIMX=%zu -DDIMY=%zu", args.cell_work, args.dimx, args.dimy);
  err = clBuildProgram(program, 0, NULL, options, NULL, NULL);
  if (err == CL_BUILD_PROGRAM_FAILURE)
  {
    size_t len;
    char buffer[2048];

    printf("OpenCL build log:\n");
    clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
    printf("%s\n", buffer);
  }
  check_error(err, "Building program");

  cl_kernel triad_1 = clCreateKernel(program, "triad_1", &err);
  check_error(err, "Creating kernel 1");

  cl_kernel triad_2 = clCreateKernel(program, "triad_2", &err);
  check_error(err, "Creating kernel 2");

  cl_kernel triad_3 = clCreateKernel(program, "triad_3", &err);
  check_error(err, "Creating kernel 3");

  cl_kernel triad_4 = clCreateKernel(program, "triad_4", &err);
  check_error(err, "Creating kernel 4");

  // Init host memory
  double *h_a = malloc(sizeof(double)*args.cell_work*args.dimx*args.dimy);
  double *h_b = malloc(sizeof(double)*args.cell_work*args.dimx*args.dimy);
  double *h_x = malloc(sizeof(double)*args.cell_work*args.dimx*args.dimy*4);
  double *h_y = malloc(sizeof(double)*args.cell_work*args.dimx*args.dimy*4);

  for (int j = 0; j < args.dimy; j++)
    for (int i = 0; i < args.dimx; i++)
      for (int a = 0; a < args.cell_work; a++)
      {
        size_t index = a + args.cell_work * (i + args.dimx * j);
        h_a[index] = 2.0;
        h_b[index] = 3.0;
      }

  for (int o = 0; o < 4; o++)
    for (int j = 0; j < args.dimy; j++)
      for (int i = 0; i < args.dimx; i++)
        for (int a = 0; a < args.cell_work; a++)
        {
          size_t index = a + args.cell_work * (i + args.dimx * (j + args.dimy*o));
          h_x[index] = 1.0;
          h_y[index] = 0.0;
        }


  // Init device memory
  cl_mem d_a, d_b;
  cl_mem d_x[4], d_y[4];
  d_a = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(double)*args.cell_work*args.dimx*args.dimy, NULL, &err);
  check_error(err, "Creating d_a buffer");

  d_b = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(double)*args.cell_work*args.dimx*args.dimy, NULL, &err);
  check_error(err, "Creating d_b buffer");

  for (int o = 0; o < 4; o++)
  {
    d_x[o] = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(double)*args.cell_work*args.dimx*args.dimy, NULL, &err);
    check_error(err, "Creating d_x buffer");

    d_y[o] = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(double)*args.cell_work*args.dimx*args.dimy, NULL, &err);
    check_error(err, "Creating d_y buffer");
  }


  // Copy over host memory
  err = clEnqueueWriteBuffer(queue, d_a, CL_FALSE, 0, sizeof(double)*args.cell_work*args.dimx*args.dimy, h_a, 0, NULL, NULL);
  check_error(err, "Copying over a");
  err = clEnqueueWriteBuffer(queue, d_b, CL_FALSE, 0, sizeof(double)*args.cell_work*args.dimx*args.dimy, h_b, 0, NULL, NULL);
  check_error(err, "Copying over b");
  for (int o = 0; o < 4; o++)
  {
    err = clEnqueueWriteBuffer(queue, d_x[o], CL_FALSE, 0, sizeof(double)*args.cell_work*args.dimx*args.dimy, h_x+args.cell_work*args.dimx*args.dimy*o, 0, NULL, NULL);
    check_error(err, "Copying over x");
    err = clEnqueueWriteBuffer(queue, d_y[o], CL_FALSE, 0, sizeof(double)*args.cell_work*args.dimx*args.dimy, h_y+args.cell_work*args.dimx*args.dimy*o, 0, NULL, NULL);
    check_error(err, "Copying over y");
  }

  unsigned int num_planes;
  struct plane *planes;
  init_planes(args.dimx, args.dimy, &planes, &num_planes);
  //print_planes(planes, num_planes);
  cl_mem *d_planes = malloc(sizeof(cl_mem)*num_planes);
  copy_planes(d_planes, planes, num_planes, context);

  err = clFinish(queue);
  check_error(err, "finish before start");

  double timings[4][ITERS][4];

  //*****************************************************************
  // Access option 1
  // a and b in i,j order
  // x and y in i,j order
  //*****************************************************************

  for (int it = 0; it < ITERS; it++)
  {
    unsigned oct = 3;
    int istep = -1;
    int jstep = -1;
    for (int jstep = -1; jstep < 2; jstep += 2)
      for (int istep = -1; istep < 2; istep += 2)
      {
        double time1 = wtime();

        for (unsigned p = 0; p < num_planes; p++)
        {
          err  = clSetKernelArg(triad_1, 0, sizeof(unsigned int), &oct);
          err |= clSetKernelArg(triad_1, 1, sizeof(int), &istep);
          err |= clSetKernelArg(triad_1, 2, sizeof(int), &jstep);
          err |= clSetKernelArg(triad_1, 3, sizeof(cl_mem), &d_planes[p]);
          err |= clSetKernelArg(triad_1, 4, sizeof(cl_mem), &d_a);
          err |= clSetKernelArg(triad_1, 5, sizeof(cl_mem), &d_b);
          err |= clSetKernelArg(triad_1, 6, sizeof(cl_mem), &d_x[oct]);
          err |= clSetKernelArg(triad_1, 7, sizeof(cl_mem), &d_y[oct]);
          check_error(err, "Setting args");

          const size_t global[2] = {args.cell_work, planes[p].num_cells};
          err = clEnqueueNDRangeKernel(queue, triad_1, 2, NULL, global, NULL, 0, NULL, NULL);
          check_error(err, "Enqueueing kernel 1");
        }
        err = clFinish(queue);
        check_error(err, "Finish octant");

        double time2 = wtime();
        timings[0][it][oct] = time2-time1;

        oct -= 1;
      }
  }

  //*****************************************************************
  // Access option 2
  // a and b in i,j order
  // x and y in sweep order
  //*****************************************************************

  // Cumulative number of cells in previous plane
  unsigned int *cell_offset = malloc(sizeof(unsigned int)*num_planes);
  cell_offset[0] = 0;
  for (unsigned int p = 1; p < num_planes; p++)
    cell_offset[p] = cell_offset[p-1] + planes[p-1].num_cells;

  for (int it = 0; it < ITERS; it++)
  {
    unsigned oct = 3;
    int istep = -1;
    int jstep = -1;
    for (int jstep = -1; jstep < 2; jstep += 2)
      for (int istep = -1; istep < 2; istep += 2)
      {
        double time1 = wtime();

        for (unsigned p = 0; p < num_planes; p++)
        {
          err  = clSetKernelArg(triad_2, 0, sizeof(unsigned int), &oct);
          err |= clSetKernelArg(triad_2, 1, sizeof(int), &istep);
          err |= clSetKernelArg(triad_2, 2, sizeof(int), &jstep);
          err |= clSetKernelArg(triad_2, 3, sizeof(unsigned int), &cell_offset[p]);
          err |= clSetKernelArg(triad_2, 4, sizeof(cl_mem), &d_planes[p]);
          err |= clSetKernelArg(triad_2, 5, sizeof(cl_mem), &d_a);
          err |= clSetKernelArg(triad_2, 6, sizeof(cl_mem), &d_b);
          err |= clSetKernelArg(triad_2, 7, sizeof(cl_mem), &d_y[oct]);
          err |= clSetKernelArg(triad_2, 8, sizeof(cl_mem), &d_x[oct]);
          check_error(err, "Setting args");

          const size_t global[2] = {args.cell_work, planes[p].num_cells};
          err = clEnqueueNDRangeKernel(queue, triad_2, 2, NULL, global, NULL, 0, NULL, NULL);
          check_error(err, "Enqueueing kernel 2");
        }
        err = clFinish(queue);
        check_error(err, "Finish octant");

        double time2 = wtime();
        timings[1][it][oct] = time2-time1;

        oct -= 1;
      }
  }


  //*****************************************************************
  // Access option 3
  // a and b in sweep 4 order
  // x and y in sweep order
  //*****************************************************************

  // Make a list of where the data cells are located in the data array
  // for octant 2 where the data is in octant 4 order
  unsigned int **data_lookup = malloc(sizeof(unsigned int *)*num_planes);
  for (unsigned int p = 0; p < num_planes; p++)
    data_lookup[p] = malloc(sizeof(unsigned int)*planes[p].num_cells);

  unsigned int *index = malloc(sizeof(unsigned int)*num_planes);
  for (unsigned int p = 0; p < num_planes; p++)
    index[p] = 0;

  // loop over sweep 2 saving the position in sweep 4
  for (unsigned int p = 0; p < num_planes; p++)
    for (unsigned int c = 0; c < planes[p].num_cells; c++)
    {
      unsigned int i = planes[p].cells[c].i;
      unsigned int j = args.dimy - planes[p].cells[c].j - 1;
      data_lookup[p][c] = cell_offset[i+j] + index[i+j];
      index[i+j] += 1;
    }

  cl_mem *d_data_location = malloc(sizeof(cl_mem)*num_planes);
  for (unsigned int p = 0; p < num_planes; p++)
  {
    d_data_location[p] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(unsigned int)*planes[p].num_cells, data_lookup[p], &err);
    check_error(err, "Creating data location buffer");
  }

  err = clFinish(queue);
  check_error(err, "Finish queue after data copy");

  for (int it = 0; it < ITERS; it++)
  {
    unsigned oct = 3;
    for (int jstep = -1; jstep < 2; jstep += 2)
      for (int istep = -1; istep < 2; istep += 2)
      {
        double time1 = wtime();

        for (unsigned p = 0; p < num_planes; p++)
        {
          err  = clSetKernelArg(triad_3, 0, sizeof(unsigned int), &oct);
          err |= clSetKernelArg(triad_3, 1, sizeof(int), &istep);
          err |= clSetKernelArg(triad_3, 2, sizeof(int), &jstep);
          err |= clSetKernelArg(triad_3, 3, sizeof(unsigned int), &cell_offset[p]);
          err |= clSetKernelArg(triad_3, 4, sizeof(cl_mem), &d_planes[p]);
          err |= clSetKernelArg(triad_3, 5, sizeof(cl_mem), &d_data_location[p]);
          err |= clSetKernelArg(triad_3, 6, sizeof(cl_mem), &d_a);
          err |= clSetKernelArg(triad_3, 7, sizeof(cl_mem), &d_b);
          err |= clSetKernelArg(triad_3, 8, sizeof(cl_mem), &d_x[oct]);
          err |= clSetKernelArg(triad_3, 9, sizeof(cl_mem), &d_y[oct]);
          check_error(err, "Setting args");

          const size_t global[2] = {args.cell_work, planes[p].num_cells};
          err = clEnqueueNDRangeKernel(queue, triad_3, 2, NULL, global, NULL, 0, NULL, NULL);
          check_error(err, "Enqueueing kernel 3");
        }
        err = clFinish(queue);
        check_error(err, "Finish octant");

        double time2 = wtime();
        timings[2][it][oct] = time2-time1;

        oct -= 1;
      }
  }

  //*****************************************************************
  // Access option 4
  // a and b in sweep order, reordered for each sweep
  // x and y in sweep order
  //*****************************************************************

  double *h_a_tmp = malloc(sizeof(double)*args.cell_work*args.dimx*args.dimy);
  double *h_b_tmp = malloc(sizeof(double)*args.cell_work*args.dimx*args.dimy);

  for (int it = 0; it < ITERS; it++)
  {
    unsigned oct = 3;
    for (int jstep = -1; jstep < 2; jstep += 2)
      for (int istep = -1; istep < 2; istep += 2)
      {
        double time1 = wtime();

        if (oct == 3 || oct == 0)
        {
          // Reorder for octant 4
          // Octant 1 is this backwards
          // A
          for (unsigned p = 0; p < num_planes; p++)
            for (unsigned c = 0; c < planes[p].num_cells; c++)
            {
              size_t i = args.dimx - planes[p].cells[c].i - 1;
              size_t j = args.dimy - planes[p].cells[c].j - 1;
              for (unsigned l = 0; l < args.cell_work; l++)
              {
                h_a_tmp[l + args.cell_work * (c + cell_offset[p])] =
                  h_a[l + args.cell_work * (i + args.dimx * j)];
              }
            }
            err = clEnqueueWriteBuffer(queue, d_a, CL_FALSE, 0, sizeof(double)*args.cell_work*args.dimx*args.dimy, h_a_tmp, 0, NULL, NULL);
            check_error(err, "Copying reordered a");
            // B
            for (unsigned p = 0; p < num_planes; p++)
              for (unsigned c = 0; c < planes[p].num_cells; c++)
              {
                size_t i = args.dimx - planes[p].cells[c].i - 1;
                size_t j = args.dimy - planes[p].cells[c].j - 1;
                for (unsigned l = 0; l < args.cell_work; l++)
                {
                  h_b_tmp[l + args.cell_work * (c + cell_offset[p])] =
                    h_b[l + args.cell_work * (i + args.dimx * j)];
                }
              }
              err = clEnqueueWriteBuffer(queue, d_b, CL_FALSE, 0, sizeof(double)*args.cell_work*args.dimx*args.dimy, h_b_tmp, 0, NULL, NULL);
              check_error(err, "Copying reordered b");
        }
        else if (oct == 2)
        {
          // Reorder for octant 3
          // Octant 2 is this backwards
          // (octant 2 is straight after so don't need to reorder)
          for (unsigned p = 0; p < num_planes; p++)
            for (unsigned c = 0; c < planes[p].num_cells; c++)
            {
               size_t i = planes[p].cells[c].i;
               size_t j = args.dimy - planes[p].cells[c].j - 1;
               for (unsigned l = 0; l < args.cell_work; l++)
               {
                 h_a_tmp[l + args.cell_work * (c + cell_offset[p])] =
                   h_a[l + args.cell_work * (i + args.dimx * j)];
               }
            }
            err = clEnqueueWriteBuffer(queue, d_a, CL_FALSE, 0, sizeof(double)*args.cell_work*args.dimx*args.dimy, h_a_tmp, 0, NULL, NULL);
            check_error(err, "Copying reordered a");
            //B
            for (unsigned p = 0; p < num_planes; p++)
              for (unsigned c = 0; c < planes[p].num_cells; c++)
              {
                size_t i = planes[p].cells[c].i;
                size_t j = args.dimy - planes[p].cells[c].j - 1;
                for (unsigned l = 0; l < args.cell_work; l++)
                {
                   h_b_tmp[l + args.cell_work * (c + cell_offset[p])] =
                    h_b[l + args.cell_work * (i + args.dimx * j)];
                }
              }
              err = clEnqueueWriteBuffer(queue, d_b, CL_FALSE, 0, sizeof(double)*args.cell_work*args.dimx*args.dimy, h_b_tmp, 0, NULL, NULL);
              check_error(err, "Copying reordered b");
        }

        for (unsigned p = 0; p < num_planes; p++)
        {
          err  = clSetKernelArg(triad_4, 0, sizeof(unsigned int), &oct);
          err |= clSetKernelArg(triad_4, 1, sizeof(int), &istep);
          err |= clSetKernelArg(triad_4, 2, sizeof(int), &jstep);
          err |= clSetKernelArg(triad_4, 3, sizeof(unsigned int), &cell_offset[p]);
          err |= clSetKernelArg(triad_4, 4, sizeof(cl_mem), &d_planes[p]);
          err |= clSetKernelArg(triad_4, 5, sizeof(cl_mem), &d_data_location[p]);
          err |= clSetKernelArg(triad_4, 6, sizeof(cl_mem), &d_a);
          err |= clSetKernelArg(triad_4, 7, sizeof(cl_mem), &d_b);
          err |= clSetKernelArg(triad_4, 8, sizeof(cl_mem), &d_y[oct]);
          err |= clSetKernelArg(triad_4, 9, sizeof(cl_mem), &d_x[oct]);
          check_error(err, "Setting args");

          const size_t global[2] = {args.cell_work, planes[p].num_cells};
          err = clEnqueueNDRangeKernel(queue, triad_4, 2, NULL, global, NULL, 0, NULL, NULL);
          check_error(err, "Enqueueing kernel 4");
        }
        err = clFinish(queue);
        check_error(err, "Finish octant");

        double time2 = wtime();
        timings[3][it][oct] = time2-time1;

        oct -= 1;
      }
  }




  //*****************************************************************
  // Check results
  // y should be 5 after kernel 1
  // x should be 13 after kernel 2
  // y should be 29 after kernel 3
  // x should be 61 after kernel 4
  //*****************************************************************

    const double answer = 61.0;

    for (int o = 0; o < 4; o++)
    {
      err = clEnqueueReadBuffer(queue, d_x[o], CL_FALSE, 0, sizeof(double)*args.cell_work*args.dimx*args.dimy, h_x+args.cell_work*args.dimx*args.dimy*o, 0, NULL, NULL);
      check_error(err, "Reading back x");
    }
    err = clFinish(queue);
    check_error(err, "Finish queue after read");
    bool correct[4] = {true, true, true, true};
    int oct = 0;
    for (unsigned int oct = 0; oct < 4; oct++)
      for (unsigned j = 0; j < args.dimy; j++)
        for (unsigned i = 0; i < args.dimx; i++)
          for (unsigned a = 0; a < args.cell_work; a++)
            if (h_x[a + args.cell_work * (i + args.dimx * (j + args.dimy*oct))] != answer)
            {
              correct[oct] = false;
            }

    if (correct[0] && correct[1] && correct[2] && correct[3])
      printf("\x1B[32mAnswers correct\x1B[0m\n");
    else
    {
      printf("\x1B[31mAnswers incorrect\x1B[0m\n");
      for (int o = 0; o < 4; o++)
        if(!correct[o])
          printf("  \x1B[31mOctant %d incorrect\x1B[0m\n", o+1);
    }

  //*****************************************************************
  // Print timings
  //*****************************************************************

  printf("\n");
  printf("Timing report\n");

  // Options
  for (int opt = 0; opt < 4; opt++)
  {
    double min[4] = {DBL_MAX, DBL_MAX, DBL_MAX, DBL_MAX};
    double max[4] = {-DBL_MAX, -DBL_MAX, -DBL_MAX, -DBL_MAX};
    double avg[4] = {0.0, 0.0, 0.0, 0.0};
    for (int it = 1; it < ITERS; it++)
    {
      for (int o = 0; o < 4; o++)
      {
        min[o] = MIN(min[o], timings[opt][it][o]);
        max[o] = MAX(max[o], timings[opt][it][o]);
        avg[o] += timings[opt][it][o];
      }
    }
    printf("  Option %d\n", opt+1);
    printf("    %-8s %-8s %-8s %-8s\n", "Oct", "Min",  "Max",  "Avg");
    for (int o = 3; o >= 0; o--)
    {
      printf("    %-8d %-8f %-8f %-8f\n", o+1, min[o], max[o], avg[o]/(ITERS-1.0));
    }
    double total[3] = {0.0, 0.0, 0.0};
    for (int o = 0; o < 4; o++)
    {
      total[0] += min[o];
      total[1] += max[o];
      total[2] += avg[o]/(ITERS-1.0);
    }
    printf("    %-8s %-8f %-8f %-8f\n", "Total", total[0], total[1], total[2]);
    printf("\n");
  }


  // Finish OpenCL
  clReleaseMemObject(d_a);
  clReleaseMemObject(d_b);
  for (int o = 0; o < 4; o++)
  {
    clReleaseMemObject(d_x[o]);
    clReleaseMemObject(d_y[o]);
  }
  clReleaseProgram(program);
  clReleaseKernel(triad_1);
  clReleaseKernel(triad_2);
  clReleaseCommandQueue(queue);
  clReleaseContext(context);

  free(h_a);
  free(h_b);
  free(h_x);
  free(h_y);

  return EXIT_SUCCESS;

}

void init_planes(const unsigned int dimx, const unsigned int dimy, struct plane ** planes, unsigned int *num_planes)
{
  *num_planes = dimx + dimy - 1;
  *planes = malloc(sizeof(struct plane) * *num_planes);

  for (unsigned int p = 0; p < *num_planes; p++)
  {
    (*planes)[p].num_cells = 0;
  }

  for (unsigned int j = 0; j < dimy; j++)
    for (unsigned int i = 0; i < dimx; i++)
      (*planes)[i+j].num_cells += 1;

  for (unsigned int p = 0; p < *num_planes; p++)
  {
    (*planes)[p].cells = malloc(sizeof(struct cell_id)*(*planes)[p].num_cells);
  }

  unsigned int index[*num_planes];
  for (unsigned int p = 0; p < *num_planes; p++)
    index[p] = 0;

  for (unsigned int j = 0; j < dimy; j++)
    for (unsigned int i = 0; i < dimx; i++)
    {
      (*planes)[i+j].cells[index[i+j]].i = i;
      (*planes)[i+j].cells[index[i+j]].j = j;
      index[i+j] += 1;
    }

}

void print_planes(struct plane * planes, unsigned int num_planes)
{
  printf("Planes: %d\n", num_planes);
  for (unsigned int p = 0; p < num_planes; p++)
  {
    printf("Plane %d: %d cells\n", p, planes[p].num_cells);
  }
}

void copy_planes(cl_mem *d_planes, struct plane *planes, unsigned int num_planes, cl_context context)
{
  cl_int err;
  for (unsigned int p = 0; p < num_planes; p++)
  {
    d_planes[p] = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(struct cell_id)*planes[p].num_cells, planes[p].cells, &err);
    check_error(err, "Creating device plane list");
  }
}

double wtime(void)
{
  struct timeval t;
  gettimeofday(&t, NULL);
  return t.tv_sec + t.tv_usec * 1.0E-6;
}

