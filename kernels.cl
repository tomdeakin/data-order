
#ifndef CELL_WORK
#define CELL_WORK 64
#endif
#ifndef DIMX
#define DIMX 1024
#endif
#ifndef DIMY
#define DIMY 1024
#endif

struct cell_id
{
  unsigned int i, j;
};

//
// Option 1
// Data and solution in i,j, order
//
kernel void triad_1(
  const unsigned int oct,
  const int istep,
  const int jstep,
  global const struct cell_id * restrict cells,
  global const double * restrict a,
  global const double * restrict b,
  global const double * restrict x,
  global double * restrict y
)
{
  size_t l = get_global_id(0);

  // Get cell index
  const size_t i = (istep > 0) ? cells[get_global_id(1)].i : DIMX - cells[get_global_id(1)].i - 1;
  const size_t j = (jstep > 0) ? cells[get_global_id(1)].j : DIMY - cells[get_global_id(1)].j - 1;

  size_t data_index = l + CELL_WORK * (i + DIMX * j);
  size_t sol_index = l + CELL_WORK * (i + DIMX * j);

  y[sol_index] = a[data_index] * x[sol_index] + b[data_index];

}

//
// Option 2
// Data in i,j order
// Solution in wavefront order
//
kernel void triad_2(
  const unsigned int oct,
  const int istep,
  const int jstep,
  const unsigned int cell_offset,
  global const struct cell_id * restrict cells,
  global const double * restrict a,
  global const double * restrict b,
  global const double * restrict x,
  global double * restrict y
)
{
  size_t l = get_global_id(0);

  // Get cell index
  const size_t i = (istep > 0) ? cells[get_global_id(1)].i : DIMX - cells[get_global_id(1)].i - 1;
  const size_t j = (jstep > 0) ? cells[get_global_id(1)].j : DIMY - cells[get_global_id(1)].j - 1;

  size_t data_index = l + CELL_WORK * (i + DIMX * j);
  size_t sol_index = l + CELL_WORK * (get_global_id(1) + cell_offset);


  y[sol_index] = a[data_index] * x[sol_index] + b[data_index];

}

//
// Option 3
// Data in wavefront 4 order
// Solution in wavefront order
//
kernel void triad_3(
  const unsigned int oct,
  const int istep,
  const int jstep,
  const unsigned int cell_offset,
  global const struct cell_id * restrict cells,
  global const unsigned int * restrict data_location,
  global const double * restrict a,
  global const double * restrict b,
  global const double * restrict x,
  global double * restrict y
)
{
  size_t l = get_global_id(0);

  // Get cell index
  const size_t i = (istep > 0) ? cells[get_global_id(1)].i : DIMX - cells[get_global_id(1)].i - 1;
  const size_t j = (jstep > 0) ? cells[get_global_id(1)].j : DIMY - cells[get_global_id(1)].j - 1;

  size_t data_index;
  if (oct == 3)
    data_index = l + CELL_WORK * (get_global_id(1) + cell_offset);
  else if (oct == 2)
    data_index = l + CELL_WORK * data_location[get_global_id(1)];
  else if (oct == 1)
    data_index = l + CELL_WORK * (DIMX*DIMY - data_location[get_global_id(1)] - 1);
  else if (oct == 0)
    data_index = l + CELL_WORK * (DIMX*DIMY - get_global_id(1) - cell_offset - 1);

  size_t sol_index = l + CELL_WORK * (get_global_id(1) + cell_offset);



  y[sol_index] = a[data_index] * x[sol_index] + b[data_index];

}

//
// Option 4
// Data in wavefront order, reordered on host
// Solution in wavefront order
//
kernel void triad_4(
  const unsigned int oct,
  const int istep,
  const int jstep,
  const unsigned int cell_offset,
  global const struct cell_id * restrict cells,
  global const unsigned int * restrict data_location,
  global const double * restrict a,
  global const double * restrict b,
  global const double * restrict x,
  global double * restrict y
)
{
  size_t l = get_global_id(0);

  // Get cell index
  const size_t i = (istep > 0) ? cells[get_global_id(1)].i : DIMX - cells[get_global_id(1)].i - 1;
  const size_t j = (jstep > 0) ? cells[get_global_id(1)].j : DIMY - cells[get_global_id(1)].j - 1;

  size_t data_index;
  if (oct == 3 || oct == 2)
    data_index = l + CELL_WORK * (get_global_id(1) + cell_offset);
  else
    data_index = l + CELL_WORK * (DIMX*DIMY - get_global_id(1) - cell_offset - 1);

  size_t sol_index = l + CELL_WORK * (get_global_id(1) + cell_offset);

  y[sol_index] = a[data_index] * x[sol_index] + b[data_index];

}

