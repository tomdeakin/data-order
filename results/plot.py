"""
Example of creating a radar chart (a.k.a. a spider or star chart) [1]_.

Although this example allows a frame of either 'circle' or 'polygon', polygon
frames don't have proper gridlines (the lines are circles instead of polygons).
It's possible to get a polygon grid by setting GRIDLINE_INTERPOLATION_STEPS in
matplotlib.axis to the desired number of vertices, but the orientation of the
polygon is not aligned with the radial axes.

.. [1] http://en.wikipedia.org/wiki/Radar_chart
"""
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.spines import Spine
from matplotlib.projections.polar import PolarAxes
from matplotlib.projections import register_projection


def radar_factory(num_vars, frame='circle'):
    """Create a radar chart with `num_vars` axes.

    This function creates a RadarAxes projection and registers it.

    Parameters
    ----------
    num_vars : int
        Number of variables for radar chart.
    frame : {'circle' | 'polygon'}
        Shape of frame surrounding axes.

    """
    # calculate evenly-spaced axis angles
    theta = np.linspace(0, 2*np.pi, num_vars, endpoint=False)
    # rotate theta such that the first axis is at the top
    theta += np.pi/2

    def draw_poly_patch(self):
        verts = unit_poly_verts(theta)
        return plt.Polygon(verts, closed=True, edgecolor='k')

    def draw_circle_patch(self):
        # unit circle centered on (0.5, 0.5)
        return plt.Circle((0.5, 0.5), 0.5)

    patch_dict = {'polygon': draw_poly_patch, 'circle': draw_circle_patch}
    if frame not in patch_dict:
        raise ValueError('unknown value for `frame`: %s' % frame)

    class RadarAxes(PolarAxes):

        name = 'radar'
        # use 1 line segment to connect specified points
        RESOLUTION = 2
        # define draw_frame method
        draw_patch = patch_dict[frame]

        def fill(self, *args, **kwargs):
            """Override fill so that line is closed by default"""
            closed = kwargs.pop('closed', True)
            return super(RadarAxes, self).fill(closed=closed, *args, **kwargs)

        def plot(self, *args, **kwargs):
            """Override plot so that line is closed by default"""
            lines = super(RadarAxes, self).plot(*args, **kwargs)
            for line in lines:
                self._close_line(line)

        def _close_line(self, line):
            x, y = line.get_data()
            # FIXME: markers at x[0], y[0] get doubled-up
            if x[0] != x[-1]:
                x = np.concatenate((x, [x[0]]))
                y = np.concatenate((y, [y[0]]))
                line.set_data(x, y)

        def set_varlabels(self, labels):
            self.set_thetagrids(np.degrees(theta), labels)

        def _gen_axes_patch(self):
            return self.draw_patch()

        def _gen_axes_spines(self):
            if frame == 'circle':
                return PolarAxes._gen_axes_spines(self)
            # The following is a hack to get the spines (i.e. the axes frame)
            # to draw correctly for a polygon frame.

            # spine_type must be 'left', 'right', 'top', 'bottom', or `circle`.
            spine_type = 'circle'
            verts = unit_poly_verts(theta)
            # close off polygon by repeating first vertex
            verts.append(verts[0])
            path = Path(verts)

            spine = Spine(self, spine_type, path)
            spine.set_transform(self.transAxes)
            return {'polar': spine}

    register_projection(RadarAxes)
    return theta


def unit_poly_verts(theta):
    """Return vertices of polygon for subplot axes.

    This polygon is circumscribed by a unit circle centered at (0.5, 0.5)
    """
    x0, y0, r = [0.5] * 3
    verts = [(r*np.cos(t) + x0, r*np.sin(t) + y0) for t in theta]
    return verts


def example_data():
    data = [
        ['IV', 'III', 'II', 'I'],
        ('K20X', [
            [i*1000.0 for i in[0.013971, 0.014021, 0.014082, 0.013999]],
            [i*1000.0 for i in[0.013698, 0.013684, 0.013703, 0.013715]],
            [i*1000.0 for i in[0.011946, 0.013798, 0.013906, 0.011932]]]),
        ('CS-STORM K40m', [
            [i*1000.0 for i in [0.014252, 0.014295, 0.014355, 0.014242]],
            [i*1000.0 for i in [0.013877, 0.013924, 0.013877, 0.013897]],
            [i*1000.0 for i in [0.012328, 0.013976, 0.014015, 0.012202]]]),
        ('K80', [
            [i*1000.0 for i in [0.013781, 0.013805, 0.013867, 0.013986]],
            [i*1000.0 for i in [0.013422, 0.013386, 0.013373, 0.013416]],
            [i*1000.0 for i in [0.011947, 0.013675, 0.013438, 0.011582]]]),
        ('GTX 980 Ti', [
            [i*1000.0 for i in [0.008850, 0.008714, 0.008649, 0.008814]],
            [i*1000.0 for i in [0.008540, 0.008434, 0.008443, 0.008546]],
            [i*1000.0 for i in [0.007824, 0.008605, 0.008663, 0.007804]]]),
        ('S9150', [
            [i*1000.0 for i in [0.016347, 0.016331, 0.016468, 0.016335]],
            [i*1000.0 for i in [0.013786, 0.013650, 0.013880, 0.013884]],
            [i*1000.0 for i in [0.014244, 0.015537, 0.015298, 0.012187]]]),
        ('S10k', [
            [i*1000.0 for i in [0.014437, 0.014609, 0.014608, 0.014403]],
            [i*1000.0 for i in [0.014046, 0.014043, 0.014004, 0.014022]],
            [i*1000.0 for i in [0.012714, 0.013955, 0.014394, 0.012508]]])
    ]
    return data


if __name__ == '__main__':
    N = 4
    theta = radar_factory(N, frame='polygon')

    data = example_data()
    spoke_labels = data.pop(0)

    fig = plt.figure(figsize=(9, 9))
    fig.subplots_adjust(wspace=0.25, hspace=0.20, top=0.85, bottom=0.05)

    colors = ['b', 'r', 'g']
    # Plot the four cases from the example data on separate axes
    for n, (title, case_data) in enumerate(data):
        ax = fig.add_subplot(2, 3, n + 1, projection='radar')
        # plt.rgrids([0.2, 0.4, 0.6, 0.8])
        plt.rgrids([7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17])
        ax.set_title(title, weight='bold', size='medium', position=(0.5, 1.1),
                     horizontalalignment='center', verticalalignment='center')
        for d, color in zip(case_data, colors):
            ax.plot(theta, d, color=color)
            ax.fill(theta, d, facecolor=color, alpha=0.25)
        ax.set_varlabels(spoke_labels)
        ax.set_ylim(bottom=7.0)

    # add legend relative to top-left plot
    plt.subplot(2, 3, 1)
    labels = ('Data i,j; Sol i,j', 'Data i,j; Sol oct', 'Data oct 4; Sol oct')
    legend = plt.legend(labels, loc=(-0.5, 1.3), labelspacing=0.1)
    plt.setp(legend.get_texts(), fontsize='small')

    plt.figtext(0.5, 0.965, 'Sweep time for 136 work per cell on 512x512 grid',
                ha='center', color='black', weight='bold', size='large')
    plt.figtext(0.5, 0.5, 'Times in milliseconds',
                ha='center', color='black', weight='bold', size='large')
    # plt.show()
    plt.savefig('data-order.pdf')

