
CC = icc
CFLAGS = -std=c99 -O3
OCL = -framework OpenCL



data-order: data-order.c ocl_utils.c ocl_utils.h
	$(CC) $(CFLAGS) data-order.c ocl_utils.c -o $@ $(OCL)

clean:
	rm -f data-order

